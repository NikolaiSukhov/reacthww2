
import React from 'react';

export default function Star({ numberstar, onClick }) {
  return (
    <div className="star" onClick={onClick}>
      <span role="img" aria-label="star">
        ⭐
      </span>
      <span className="star-number">{numberstar}</span>
    </div>
  );
}
