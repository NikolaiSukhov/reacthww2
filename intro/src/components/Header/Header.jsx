import Button from "../Button/Button"
import Cart from "./Cart"
import FetchData from "../Fetch/DataFetch";
import React, { useState, useEffect } from 'react';
import StarWindow from '../StarWindow/StarWindow'
import { Routes, Route, Link} from "react-router-dom";

import Star from './Star'
import  CartWindow  from '../CartWindow/CartWindow'
export default function Header({numberAdded, numberClicked,fetch}){
    const itemCount = JSON.parse(localStorage.getItem('cartItems'))?.length || 0;
    const starCount = JSON.parse(localStorage.getItem('starItem'))?.length || 0;
return(
    <>
    <header className="header">
    <div className="wrapper-header">
<Link to="/fetch" className="product-link">All Products</Link>
<Link to="/cart" className="cart-link"><Cart Number={itemCount} /></Link>
<Link to='/star' classname="star-widnow"> <Star numberstar={starCount}/></Link>
</div>
</header>

<Routes>
    <Route path="/cart" element= {<CartWindow/>}/>
    <Route path='/fetch' element= {fetch} />
    <Route path='/star' element= {<StarWindow/>} />
</Routes>
 </>
)
}