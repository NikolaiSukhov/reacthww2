export default function ModalWrapper( {children, onClick}){
    return (
        <div className="ModalWrapper" onClick={onClick} >
         
             {children}
        </div>
       
    )
}   