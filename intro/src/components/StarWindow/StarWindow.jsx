import React, { useState, useEffect } from 'react';
import Button from '../Button/Button';

export default function StarWindow() {
    const [storedStar, setStoredStar] = useState([]);
    const [star, setStar] = useState([]);

    useEffect(() => {
        const storedstar = JSON.parse(localStorage.getItem('starItem')) || [];
        setStoredStar(storedstar);
    }, []); 

    const deletestar = (index) => {
        const updatedStar = [...storedStar];
        updatedStar.splice(index, 1);
        setStoredStar(updatedStar);
        localStorage.setItem('starItem', JSON.stringify(updatedStar));
    }

    return (
        <>
            <p>Added to Favorites:</p>
            <div className="staradded">
                {storedStar.map((item, index) => (
                    <div key={index} className="storeditem">
                        <p className='starfav'> {item.Name}</p>
                        <Button onClick={() => deletestar(index)} children={'delete'} />
                    </div>
                ))}
            </div>
        </>
    );
}
