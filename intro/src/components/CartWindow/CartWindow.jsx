import React, { useState, useEffect } from 'react';
import Modal from '../Fetch/Modal';
import Button from '../Button/Button'
export default function CartWindow (){

const[storedItem, setStoredItem] = useState([])
useEffect(()=>{
  const storeditem = JSON.parse(localStorage.getItem('cartItems')) || [];
  setStoredItem(storeditem)
})
   const handleDelete = (index)=>{
    const updateItems = [...storedItem]
    updateItems.splice(index, 1)
    setStoredItem(updateItems)
    localStorage.setItem('cartItems', JSON.stringify(updateItems));
   }



    return (
        <>
        <div className="wrapper-cartwindow">
          <p className="carttext"> products:</p>
       <div className="cart-window">
        
<div className="card-cart">
{storedItem.map((item, index) => (
  <div className="cart-boxed">

              <p key={index}>{item.Name}</p>
              <p>{item.Articul}</p>
              <p><img className='image-in-cart' src={item.Url} alt="product" /></p>
              <Modal child={'delete this product'} textfirst={'delete this product'} counter={handleDelete} headertype={item.Url} />
             
              </div>
            ))}
            
</div>
       

       </div>
       </div>
        </>

    )
}