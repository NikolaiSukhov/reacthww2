function Button ( {type, classNames, onClick, children,onclick2}){
    const handleClick = () => {
        if (onClick) {
            onClick();
        }
        if (onclick2) {
            onclick2();
        }
    };

    return (
        <div className="buttonWrapper" >
        <button type={type} className={classNames}  onClick={handleClick} >
            {children}
        </button>
        </div>
    )
    
}
export default Button;