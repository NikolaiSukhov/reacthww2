import React, { useState, useEffect } from 'react';
import Modal from './Modal';
import Product from './Product';
import StarProduct from './StarProduct';


export default function Fetch({ butn, close, isopn, clickedstar,setter,starset, count }) {
  const [data, setData] = useState(null);



  useEffect(() => {
    fetch('./array.json')
      .then((response) => response.json())
      .then((data) => {
        setData(data);
      });
  }, []);

  if (data === null) {
    return <div>Loading...</div>;
  }

  return (
    <>
    <div className="wrapper-c">
      <div className="wrapper-products">
        {data.map((item) => (
          <Product key={item.Articul} item={item} btn={butn} onClick={clickedstar } count={setter} starclicked={starset} staritem={item}/>
        ))}
  
      </div>
      </div>
    </>
  );
}
