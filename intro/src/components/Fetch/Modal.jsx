import React, { useState } from 'react';
import ModalWrapper from '../Modal/modalWrapper/ModalWrapper';
import ModalBody from '../Modal/ModalBody';
import ModalHeader from '../Modal/ModalHeader/ModalHeader';
import ModalClose from '../Modal/ModalClose';
import ModalText from '../Modal/ModalText';
import ModalFooter from '../Modal/ModalFooter/ModalFooter';
import Button from '../Button/Button';

export default function Modal({headertype, naming, price, counter,click,clickhandler,child,textfirst}) {
  const [isModalOpen, setModalOpen] = useState(false);


  const openModal = () => setModalOpen(true);
  const closeModal = () => setModalOpen(false);


  return (
    <div className="WRP">
      {isModalOpen && (
        <ModalWrapper onClick={closeModal}>
          <ModalBody>
            <ModalHeader children={headertype} />
            <ModalClose onClick={closeModal} />
            <ModalText h1={naming} text={price} />
            <ModalFooter
           
              firstText={textfirst}
              firstClick={counter}
              clicker = {clickhandler}
            />
          </ModalBody>
        </ModalWrapper>
      )}
      <Button onClick={openModal} onclick2={click} >{child}</Button>
    </div>
  );
}
