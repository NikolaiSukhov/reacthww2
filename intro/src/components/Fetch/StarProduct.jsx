
import React, { useState } from 'react';
import SvgStar from './SvgStar.jsx'; 

export default function StarProduct({ click, toggleChange, item }) {
    const [starColor, setStarColor] = useState('black');
const [star, setStar] = useState([])
const deletestar = (index)=>{
    const updatestar = [...star]
    updatestar.splice(index, 1)
    setStar(updatestar)
    localStorage.setItem('starItem', JSON.stringify(updatestar));
}
    const handleStarClick = () => {
        toggleChange(); 
        if (starColor === 'black') {
         
            const starItem = JSON.parse(localStorage.getItem('starItem')) || [];
            starItem.push(item);
            localStorage.setItem('starItem', JSON.stringify(starItem));
        } else {
            deletestar()
           
        }
        setStarColor(starColor === 'black' ? 'gold' : 'black'); 

    };

    return (
        <div onClick={click} className="stardiv">
            <p className='star'><SvgStar colorSetter={starColor} onClick={handleStarClick} /></p>
        </div>
    );
}
