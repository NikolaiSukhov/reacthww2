import Modal from "./Modal";
import StarProduct from "./StarProduct";
import React, { useState } from 'react';
export default function Product({ item, btn, onClick, modalheader, count ,starclicked,staritem  }) {

    const [currentColor, setColor] = useState('black');

    const toggleColor = () => {
        setColor(currentColor === 'black' ? 'gold' : 'black');
    };
    const handleStorage= ()=>{
      const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
      cartItems.push(item);
      localStorage.setItem('cartItems', JSON.stringify(cartItems));
    }
    return (
      <>
      <div key={item.Articul} className='card'>
        <p><img className='image-img' src={item.Url} alt="product" /></p>
        <h2>{item.Name}</h2>
        <p>Price: {item.Price}</p>
        <p>Articul: {item.Articul}</p>
        <p>Color: {item.color}</p>
        {btn}
        <StarProduct click={starclicked} toggleChange={toggleColor} item={staritem} />

        <Modal headertype={item.Url} naming={item.Name} price={item.Price} counter={count}  clickhandler={handleStorage} child={'buy'} textfirst={'add to cart'}  />
        
      </div>
    

      </>
    );
  }
  